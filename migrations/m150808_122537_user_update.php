<?php

use yii\db\Schema;
use yii\db\Migration;

class m150808_122537_user_update extends Migration
{
    public function up()
    {
        $this->addcolumn('User', 'name', Schema::TYPE_STRING . ' Default NULL');
        $this->addcolumn('User', 'lastName', Schema::TYPE_STRING . ' Default NULL');
        $this->addcolumn('User', 'company', Schema::TYPE_STRING . ' Default NULL');
    }

    public function down()
    {
        $this->dropColumn('User', 'name');
        $this->dropColumn('User', 'lastName');
        $this->dropColumn('User', 'company');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
