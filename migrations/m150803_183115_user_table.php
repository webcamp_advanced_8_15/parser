<?php

use yii\db\Schema;
use yii\db\Migration;

class m150803_183115_user_table extends Migration
{
    public function up()
    {
        $this->createTable('User', [
            'userId' => Schema::TYPE_PK,
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'password' => Schema::TYPE_STRING . ' NOT NULL',
            'authKey' => Schema::TYPE_STRING,
            'accessToken' => Schema::TYPE_STRING,
            'info' => Schema::TYPE_TEXT,
            'photo' => Schema::TYPE_STRING,
            ]);
    }

    public function down()
    {
        echo "m150803_183115_user_table cannot be reverted.\n";

        $this->dropTable('User');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
