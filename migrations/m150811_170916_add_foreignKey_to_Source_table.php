<?php

use yii\db\Schema;
use yii\db\Migration;

class m150811_170916_add_foreignKey_to_Source_table extends Migration
{
    public function up()
    {
        $this->addcolumn('Source', 'userId', Schema::TYPE_INTEGER);
        $this->addForeignKey('User_Source_fk','Source','userId','User','userId');
    }

    public function down()
    {
        $this->dropColumn('Source', 'userId');
        $this->dropForeignKey('User_Source_fk','Source','userId','User','userId');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
