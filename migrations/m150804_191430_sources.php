<?php

use yii\db\Schema;
use yii\db\Migration;

class m150804_191430_sources extends Migration
{
    public function up()
    {
        $this->createTable('Source', [
            'sourceId' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'url' => Schema::TYPE_STRING,
            'status' => Schema::TYPE_STRING,
            'created' => Schema::TYPE_DATETIME,
            ]);
    }

    public function down()
    {
        echo "m150804_191430_sources cannot be reverted.\n";
        $this->dropTable('Source');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
